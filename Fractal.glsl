#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform int Mode;

uniform int XSize;
uniform int YSize;
uniform int Iter;
uniform float Mag;
uniform float Xloc;
uniform float Yloc;
uniform float jxCoord;
uniform float jyCoord;

uniform float R;
uniform float B;
uniform float G;

double xd = Mag/XSize;
double yd = Mag/YSize;


/// Complex Number Operations ///

// Addition
dvec2 cadd(dvec2 a, dvec2 b) {
	dvec2 s = dvec2((a.x+b.x), (a.y+b.y));
	return s;
}

// Multiplication
dvec2 cmul(dvec2 a, dvec2 b) {
	dvec2 s = dvec2((a.x*b.x)-(a.y*b.y), (a.y*b.x)+(a.x*b.y));
	return s;
}

// Absolute Value
float cabs(dvec2 c) {
	float s = float(sqrt(c.x*c.x + c.y*c.y));
	return s;
}


/// Fractal Interation Functions ///

// Smooth final fractal value
float LogSmooth(int j, dvec2 z) {
	// return j + 1 - log(log(cabs(z))) / log(2);
	return float(j);
}

// Mandelbrot
float Mandelbrot(int iter, dvec2 c) {

	double x0 = c.x;
	double y0 = c.y;
	double x = 0.0;
	double y = 0.0;
	int j = 0;
	while ((x*x)+(y*y) <= 4 && j < iter) {
		double t = (x*x) - (y*y) + x0;
		y = (2*x*y) + y0;
		x = t;
		j++;
	}
	return j;
    /*

	int j = 0;
	dvec2 z = dvec2(0.0);
	// while (cabs(z) < 2.0 && j < iter) {
	while (j < iter && cabs(z) < 2.0) {
		z = cadd(cmul(z, z), c);
		j++;
	}
	return LogSmooth(j, z);
	*/
}

// Burning Ship
float BurningShip(int iter, dvec2 z) {
	int j = 0;
	dvec2 c = z;
	while (cabs(z) < 2.0 && j < iter) {
		if (z.x < 0.0) z.x = z.x * -1.0;
		if (z.y > 0.0) z.y = z.y * -1.0;
		z = cadd(cmul(z, z), c);
		j++;
	}
	return LogSmooth(j, z);
}

// Julia Set
float Julia(int iter, dvec2 z) {
	int j = 0;
	dvec2 c = dvec2(jxCoord, jyCoord);
	while (cabs(z) < 2.0 && j < iter) {
		z = cadd(cmul(z, z), c);
		j++;
	}
	return LogSmooth(j, z);
}

// Main fractal calculation function
float Fractal(int mode, int iter) {
	double mx = (Xloc-(Mag*0.5))+(gl_FragCoord.x*xd);
	double my = (Yloc-(Mag*0.5))+(gl_FragCoord.y*yd);
	dvec2 z = dvec2(mx, my);
	switch (mode) {
		case 0: return Mandelbrot(iter, z);
		case 1: return BurningShip(iter, z);
		case 2: return Julia(iter, z);
	}
	return 0.0;
}



void main(void) {
    float m = Fractal(Mode, Iter) * 0.01;  
    gl_FragColor = vec4(m+R, m+B, m+G, 1.0);
}

